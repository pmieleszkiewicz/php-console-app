<?php

namespace DietApp;


class ProductDAO
{
    const FILENAME = 'products.json';
    private function __construct() {}
    
    public static function saveToJson(array $products) {
        $json = json_encode($products, JSON_PRETTY_PRINT);
        file_put_contents(self::FILENAME, $json);
    }

    public static function loadProducts() {
        if (file_exists(self::FILENAME)) {
            $content = file_get_contents(self::FILENAME);
            return json_decode($content, true);
        } else {
            echo 'File not found!';
        }
    }

    public static function getProductIndex(array $products, string $id): int {
        $i = 0;
        foreach ($products as $product) {
            if ($product['id'] == $id)
               return $i;
            $i++;
        }
        return -1;
    }
}