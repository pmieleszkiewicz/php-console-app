<?php

namespace DietApp;


class Product implements \JsonSerializable
{
    private $id;
    private $name;
    private $proteins;
    private $carbs;
    private $fats;

    public function __construct(int $id, string $name, float $proteins, float $carbs, float $fats) {
        $this->id = $id;
        $this->name = $name;
        $this->proteins = $proteins;
        $this->carbs = $carbs;
        $this->fats = $fats;
    }

    public static function create(int $id) {
        echo 'Enter product name: ';
        $product_name = stream_get_line(STDIN, 1024, PHP_EOL);
        echo 'Enter proteins: ';
        $product_proteins = floatval(stream_get_line(STDIN, 1024, PHP_EOL));
        echo 'Enter carbs: ';
        $product_carbs = floatval(stream_get_line(STDIN, 1024, PHP_EOL));
        echo 'Enter fats: ';
        $product_fats = floatval(stream_get_line(STDIN, 1024, PHP_EOL));
        $product = new self($id, $product_name, $product_proteins, $product_carbs, $product_fats);
        return $product;
    }

    public function getId(): int {
        return $this->id;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'proteins' => $this->proteins,
            'carbs' => $this->proteins,
            'fats' => $this->fats
        ];
    }

    public function __toString() {
        return <<< EOT
-----------------------
Id: $this->id
Name: $this->name
Proteins: {$this->proteins}g
Carbs: {$this->carbs}g
Fats: {$this->fats}g

EOT;
    }

}