<?php

require 'vendor/autoload.php';

use DietApp\Product as Product;
use DietApp\ProductDAO as ProductDAO;

if (count($argv) != 2) {
    echo 'Invalid number of parameters!' . PHP_EOL . 'Avaiable: [add | delete | show ]';
    die();
}

$products = ProductDAO::loadProducts();

switch($argv[1]) {
    case 'add':
        $lastId = 0;
        if ($products != null) {
            $lastId = end($products)['id'];
        }
        $product = Product::create(++$lastId);
        $products[] = $product;
        ProductDAO::saveToJson($products);
        echo 'Product added!';
        break;
    case 'delete':
        echo 'ID of product to delete: ';
        $productId = intval(stream_get_line(STDIN, 1024, PHP_EOL));
        $index = ProductDAO::getProductIndex($products, $productId);
        unset($products[$index]);
        ProductDAO::saveToJson(array_values($products));
        echo 'Product deleted!';

        break;
    case 'show':
        if ($products == null) {
            echo 'Nothing to show!' . PHP_EOL . 'You can add a new product using: php app.php add';
            die();
        }
        foreach ($products as $p) {
            $product = new Product($p['id'], $p['name'], $p['proteins'], $p['carbs'], $p['fats']);
            echo $product;
        }

        break;
}